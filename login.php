

<?php
	// Start the login session
	session_start();
?>

<!DOCTYPE html>

<html lang="en">
	<head>
	    <!-- Important Author and Description Information -->
	        <meta charset="utf-8">
	        <meta name="description" content="Net Tracker utilises Brisbane City Councils open data initiative to load data on wifi hotspots around Brisbane. We then provide a platform for users to leave reviews on each location.">
	        <meta name="author" content="Brendan Surtees & Kalebh Harwin">
	        <meta name="viewport" content="width=device-width, initial-scale=1.0">

	    <!--  Title and External Links  -->
	    <title>Net Tracker | Login</title>
	    <link rel="stylesheet" type="text/css" href="CSS/main.css">
	    <link rel="stylesheet" type="text/css" href="CSS/login.css">

	    <!-- JavaScript Link -->
	    <script src="JavaScript/main.js"></script>
	    
	    <!-- Fonts and Favicon Link -->
	    <link rel="icon" type="image/png" href="images/favicon.png">
		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
			
		<!-- OpenGraph -->
	    <meta property="og:site_name" content="Net Tracker"/>
	    <meta property="og:title" content="Net Tracker | Login"/>
	    <meta property="og:description" content="Net Tracker shows you all the best free wifi locations in the city of Brisbane."/>
	    <meta property="og:type" content="Review"/>
	    <meta property="og:url" content="login.php"/>    
	</head>


    <body>
        <!-- Content before the main -->
        <?php 
        	include 'php/before_main.inc';
        	include 'db_connect.php';
        ?>

        <!-- Main page content -->
        <main>
            <h1>Login</h1>
			<?php
			// Seeing if the user is already logged in & giving them an option to logout
				if ($_SESSION["login"] == "true") {
					echo '<b>You are logged in! </b> <br>
					<form id="rego" name="log" method="POST">
						<div class="formItem">
							<button name="logout" type="submit" >Logout</button>
						</div>
					</form>';
				}
				
				else {
					echo '<p>Log in below. Not a member yet? <a href="register.php">Register now!</a></p><br>
					<form name="loginForm" method="post">
						<div class="formItem">
							<label>Username:</label><br />
							<input required name="username" type="text" title="Must be between 5 and 12 characters long. Only letters and numbers allowed - no special characters." pattern="^[a-zA-Z0-9]{5,12}$">
						</div>
						<div class="formItem">
							<label>Password:</label><br />
							<input required name="password" type="password">
						</div>
						<div class="formItem">
							<button name="login" type="submit">Login</button>
						</div>
                
					</form><br /><br />';
				}
				
				// When they logout write to the session they are logged out
				if (isset($_POST['logout'])) {
					$_SESSION["login"] = "false";
					header("Refresh:0");
				}
			?>

            <?php				
				// Preparing to send the username to the server
                if (isset($_POST['login'])) {

                	// Set login variables
                	$usr = trim($_POST['username']);
					$pw = trim($_POST['password']);

					// PHP form validation
					// If either field is empty, echo an error message and don't run the login code
					if ($usr == "" || $pw == ""){
						echo "Please enter both your username and password!";
					}
					else {
						// Select statement to return the appropriate user
						$statement = $pdo->prepare("SELECT * FROM users WHERE username = ?");
		            	$statement->execute([$_POST['username']]);
		            	$user = $statement->fetch();
						
						$localPass = $_POST['password'];
						$serverPass = $user['password'];

						// Seeing if the password and username match
		            	if ($user && password_verify($localPass, $serverPass)){
		            		// Once completed print feedback for the user and store details of the users login in a Session
							// The session data will be used in the comment page to see if the user is logged in before commenting
							echo 'You are now Logged In!';
							$_SESSION["login"] = "true";
							$_SESSION["username"] = $user['username'];
							header("Refresh:0");
		            	}
		            	
						// Login failed - Store in the session that the login failed
		            	else {
		            		echo 'INVALID - Try Again!';
							$_SESSION["login"] = "false";
		            	}
					}
            	}
	        ?>
        </main>

        <!-- Content after the main -->
        <?php include 'php/after_main.inc' ?>

    </body>
</html>