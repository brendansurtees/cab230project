<!-- Footer -->
        <footer>
            <article class="footer_box1">
                <ul>
                    <li>
                        <a href="#"><img src="images/facebook-icon.svg" alt="Facebook"/>Facebook</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/twitter-icon.svg" alt="Twitter"/>Twitter</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/rss-icon.svg" alt="RSS"/>RSS</a>
                    </li>
                </ul>
                <p>Icons made by <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></p>
                <p>© Net Tracker 2018. Net Tracker is not officially affiliated with Brisbane City Council. </p>
                </article>
                
                <article class="footer_box2">
                    <h3>Conact us either by email or phone:</h3><br>
                    <p>Email:<a href="#">  query@nettracker.com.au</a></p>
                    <p>Phone:  3211 8940</p>
            </article>
        </footer>