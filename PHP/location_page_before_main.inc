        <!-- Desktop header -->
        <header class="header desktop-header" id="header">
            <!-- Website Logo -->
            <a href="index.php"><img id="desktop-menu-img" alt="Logo" src="Images/logo.svg"/></a>
                
            <!-- Main Nav / Left -->
            <nav>
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">About</a></li>
                </ul>
            </nav>
            <!-- Rego Nav / Right -->
            <nav class="rego_nav">
                <ul>
                    <?php
                        // Check to see if they are logged in
                        if ($_SESSION["login"] == "true") {
                            echo '<form method="POST">
                                    <li>
                                        <button name="logout" type="submit" class="logout_button">Logout</button>
                                    </li>
                                </form>';
                                
                            // When they logout write to the session they are logged out
                            if (isset($_POST['logout'])) {
                                $_SESSION["login"] = "false";
                                header("Refresh:0");
                            }
                        }
                        else {
                            // If not logged in display the login and register buttons
                            echo '<li ><a href="login.php">Login</a></li>
                            <li class="nav_right_link"><a href="register.php">Register</a></li>';
                        }
                    ?>
                </ul>
            </nav>                  
        </header>
    
        <!-- Mobile header -->
        <header class="mobile-header">
            <nav>
                <ul>
                    <li id="menu-li">
                        <a href="#"><img id="menu-img" src="Images/menu-icon.png" alt="Dropdown menu" onclick="toggle_menu()" />
                        </a>
                    </li>
                    <li><a href="index.php"><img src="Images/logo.png" class="mobile_logo" alt="Net Tracker Logo"></a></li>
                    <li class="hidden-btns" id="home-button-mob"><a href="index.php">Home</a></li>
                    <li class="hidden-btns"><a href="about.php">About</a></li>
                    <?php
                        // Check to see if they are logged in
                        if ($_SESSION["login"] == "true") {
                            echo '<form method="POST">
                                    <li class="hidden-btns">
                                        <button name="logout" type="submit" class="logout_button">Logout</button>
                                    </li>
                                </form>';
                                
                            // When they logout write to the session they are logged out
                            if (isset($_POST['logout'])) {
                                $_SESSION["login"] = "false";
                                header("Refresh:0");
                            }
                        }
                        else {
                            // If not logged in display the login and register buttons
                            echo '<li class="hidden-btns"><a href="login.php">Login</a></li>
                            <li class="hidden-btns"><a href="register.php">Register</a></li>';
                        }
                    ?>
                </ul>
            </nav>
        </header>