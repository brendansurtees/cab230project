
<?php
	// Start the login session
	session_start();
?>

<!DOCTYPE html>

<html lang="en">
<head>
    <!-- Important Author and Description Information -->
        <meta charset="utf-8">
        <meta name="description" content="Net Tracker utilises Brisbane City Councils open data initiative to load data on wifi hotspots around Brisbane. We then provide a platform for users to leave reviews on each location.">
        <meta name="author" content="Brendan Surtees & Kalebh Harwin">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!--  Title and External Links  -->
		<title>Net Tracker | WiFi Hotspot</title>
		<link rel="stylesheet" type="text/css" href="CSS/main.css">
		<link rel="stylesheet" type="text/css" href="CSS/location.css">
	
		<!-- JavaScript Link -->
		<script src="JavaScript/main.js"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB86ADoIvm-TDLPTPY_rLNUrAc7hEftL0E&callback=wifiMap"></script>
	
	   <!-- Fonts and Favicon Link -->
		<link rel="icon" type="image/png" href="images/favicon.png">
		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
		
		<!-- OpenGraph -->
	    <meta property="og:site_name" content="Net Tracker"/>
	    <meta property="og:title" content="Wifi Location"/>
	    <meta property="og:description" content="Net Tracker shows you all the best free wifi locations in the city of Brisbane."/>
	    <meta property="og:type" content="Review"/>
	    <meta property="og:url" content="wifilocation.php"/>
	</head>
	
	<?php
		// Setting variables for the each result element
		$title = $_GET['locationName'];
		$location = $_GET['addressName'];
		$suburbLocation = $_GET['suburbName'];
		$lon = $_GET['longitude'];
		$lati = $_GET['latitude'];
	?>
	
	<body onload="wifiMap()">
		<!-- Content before the main -->
		<?php
			include 'php/location_page_before_main.inc';
			include 'db_connect.php';
		?>
		
		<!--  Displaying the map -->
		<div id="map" style="height: 60vh; width: 100%;"></div>
		<script>
			// Creating the map
			// Code sourced from Google - https://developers.google.com/maps/documentation/javascript/examples/marker-simple
			function wifiMap() {
			<?php echo 'var wifiLocation = new google.maps.LatLng('. $lati .', '. $lon .');' ?>
				var map = new google.maps.Map(document.getElementById('map'), {
				  zoom: 18,
				  center: wifiLocation,
				  icon: 'images/marker.png'
				});
	
				// Creating the marker on the map
				var marker = new google.maps.Marker({
				  position: wifiLocation,
				  disableDefaultUI: true,
				  map: map,
				  icon: 'images/marker.png',
				  //icon: '../images/marker.png',
				  <?php echo "title: '" . $title ."'" ?>
				});
			}
		</script>

        <!-- Main page content -->
        <main>
            <?php      
            echo '<h1>' . $title . '</h1>';
            echo '<h2>' . $location .', '. $suburbLocation . '</h2>';
            ?>
		
			<!--  Text sourced from Brisbane City Council -->
            <p>Free wireless internet (Wi-Fi) is one way Brisbane City Council is creating a more connected and accessible city.  Free Wi-Fi is now available in 30 parks and public spaces across Brisbane, including the Queen Street Mall, Reddacliff Place, Victoria Bridge, South Bank Parklands, Roma Street Parkland, Valley Malls, Mt Coot-tha Summit Lookout, Brisbane Libraries and on CityCats. <br><br>Council has expanded its free Wi-Fi service across the Brisbane Central Business District (CBD) and into the vibrant precincts of James Street and Newstead, Caxton Street and Given Terrace, Stones Corner, Sandgate town centre, Wynnum town centre and St Lucia village allowing residents and visitors to stay connected as they move around Brisbane.</p>

            <!-- Geographic microdata -->
            <?php echo
        		'<div itemscope itemtype="http://schema.org/Place">
        			<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
     		    		<meta itemprop="latitude" content="'.$lati.'" />
						<meta itemprop="longitude" content="'.$lon.'" />
     				</div>
 				</div>'
         	?>


			<!-- Comments Section -->
            <h2>Comments</h2>
            <br>
            <form id="rego" name="commentsArea" method="POST">
				<textarea name="commentsText" cols="100" rows="10" placeholder="Type your comment here and then click the comment button..."></textarea>
				<div class="formItem">
					<b>Location Star Rating:</b>
					<select name="stars">
						<option value="5">5</option>
						<option value="4">4</option>
						<option value="3">3</option>
						<option value="2">2</option>
						<option value="1">1</option>
					</select>
				</div>

				<!-- Submit Comment -->
                <div class="formItem">
                    <button name="comment" type="submit" >Comment</button>
                </div>
            </form>
            
            <?php
            //Check to see if comments have been posted for this location
            $selectquery = $pdo->prepare("SELECT * FROM comments");
            $selectquery->execute();
            $existingcomments = $pdo->prepare("SELECT * FROM comments");
            $existingcomments->execute();
            
			//For Each comment in the database print each comment
            foreach($existingcomments as $written) {
                if ($written['hotspotName'] == $title) {
                    echo '<article class="comments">
                                <h4>'.$written['username'].'</h4>
                                <h5>This has a '.$written['rating'].' star rating.</h5>
                                <p>'.$written['comment'].'</p>
                            </article>';
                }
            }
            
            // Checking to see if they user is logged in before commenting
                if (isset($_POST['comment'])) {
                    if ($_SESSION['login'] == "true") {
                        
                        $comment = $_POST['commentsText'];
                        $rating = $_POST['stars'];
                        $username = $_SESSION["username"];
						
						// Averaging Ratings for Locations
						$oldratingQuery = "SELECT rating FROM dataset WHERE hotspotName = '$title'";
						$oldrating = $pdo->query($oldratingQuery);
						
						$AVGrating = round((($rating + $oldrating) / 2));
                        
                        // Prepare the Insert & Update statements
                        $query = $pdo->prepare("INSERT INTO comments (username, hotspotName, rating, comment) VALUES ('$username', '$title', '$rating', '$comment' )");
						$update = $pdo->prepare("UPDATE dataset SET rating = '$AVGrating' WHERE hotspotName = '$title'");
                        
                        // Run the statements
                        $query->execute();
						$update->execute();
                        
						//Printing the comment
                        echo '<article class="comments">
                            <h4>'.$username.'</h4>
                            <h5>This has a '.$rating.' star rating.</h5>
                            <p>'.$comment.'</p>
                        </article>';
                    }
                    
					//Notify the User they need to login
                    else {
                        echo "<script type='text/javascript'>alert('You need to Login to Comment!');</script>";
                    }
                }

            ?>
        </main>

        <!-- Content after the main -->
        <?php include 'php/after_main.inc' ?>

    </body>
</html>