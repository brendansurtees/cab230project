
<?php
	// Start the login session
	session_start();
?>

<!DOCTYPE html>

<html lang="en">
    <head>
        <!-- Important Author and Description Information -->
        <meta charset="utf-8">
        <meta name="description" content="Net Tracker utilises Brisbane City Councils open data initiative to load data on wifi hotspots around Brisbane. We then provide a platform for users to leave reviews on each location.">
        <meta name="author" content="Brendan Surtees & Kalebh Harwin">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--  Title and External Links  -->
        <title>Net Tracker | Register</title>
        <link rel="stylesheet" type="text/css" href="CSS/main.css">
        <link rel="stylesheet" type="text/css" href="CSS/register.css">

        <!-- JavaScript Link -->
        <script src="JavaScript/main.js"></script>

        <!-- Fonts and Favicon Link -->
        <link rel="icon" type="image/png" href="images/favicon.png">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        
        <!-- OpenGraph -->
        <meta property="og:site_name" content="Net Tracker"/>
        <meta property="og:title" content="Net Tracker | Register"/>
        <meta property="og:description" content="Register for a Net Tracker account today! Net Tracker shows you all the best free wifi locations in the city of Brisbane."/>
        <meta property="og:type" content="Review"/>
        <meta property="og:url" content="register.php"/>          
    </head>


    <body>
    <!-- Content before the main -->
        <?php 
            include 'php/before_main.inc';
            include 'db_connect.php';
        ?>

        <!-- Main page content -->
        <main>

            <!-- Connect to database -->
            <h1 id="pageTitle">Register</h1>
            <div id="pageInfo">
                <p>Not a member? Enter your details below to register.</p>
                <p style="font-size:11px;">All fields marked with an asterix (*) are required.</p>
            </div>

            <!-- Registration form -->
            <form id="rego" name="registrationForm" method="POST">
                <div class="formItem">
                    <label>Email:*</label><br />
                    <input required name="email" type="email" placeholder="johnsmith@email.com" title="Please enter a valid email address.">
                </div>
                <div class="formItem">
                    <label>Date of Birth:*</label><br />
                    <input type="text" name="DOB" placeholder="YYYY-MM-DD" required pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))" title="Enter a date in this format YYYY-MM-DD"/>
                </div>
                <div class="formItem">
                    <label>Gender:*</label><br /><br />
                    <input type="radio" name="gender" value="male">
                    <label>Male </label>
                    <input type="radio" name="gender" value="female">
                    <label>Female </label><br />
                </div>
                <div class="formItem">
                    <label>Username:*</label><br />
                    <input required name="username" type="text" title="Must be between 5 and 12 characters long. Only letters and numbers allowed - no special characters." pattern="^[a-zA-Z0-9]{5,12}$">
                </div>
                <div class="formItem">
                    <label>Password:*<span style="font-size:11px;">4-8 characters. Must include one numeral.</span></label><br />
                    <input required name="password" type="password" title="Must be from 4-8 characters long, and include at least one numeral." pattern="^(?=.*\d).{4,8}$">
                </div>
                <div class="formItem">
                    <button name="register" type="submit">Register</button>
                </div>

            </form>

            <?php
                //Prepare the Insert statement
                if (isset($_POST['register'])) {
                    
                    // Sets user info as variables
                    $username = $_POST['username'];
                    $password = $_POST['password'];
                    $email = $_POST['email'];
                    $dob = $_POST['DOB'];
                    $gender = $_POST['gender'];
    				
                    // PHP validation
                    if ($email == ""){ echo "Please enter an email!"; }
                    else if (!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email)){
                        echo "Please enter a valid email address";
                    }
                    else if ($dob == ""){ echo "Please enter a date of birth"; }
                    else if(!preg_match("/(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))/", $dob)){
                        echo "Enter a date in this format YYYY-MM-DD";
                    }
                    else if ($gender == ""){
                        echo "Please select a gender";
                    }
                    else if ($username == ""){ echo "Please enter a username!"; }
                    else if (!preg_match("/[a-zA-Z0-9]{5,12}/", $username)){
                        echo "Username must be between 5 and 12 characters long. Only letters and numbers allowed - no special characters.";
                    }
                    else if ($password == ""){ echo "Please enter a password!"; }
                    else if (!preg_match("/(?=.*\d).{4,8}/", $password)){
                        echo "Password must be from 4-8 characters long, and include at least one numeral.";
                    }
                    
                    // If there are no PHP validation issues, add the user to the database
                    else{
                        // Query the Check statement to see if username already exists
                        $user_query = $pdo->query("SELECT * FROM users WHERE username='".$username."'");
                        
                        // See if any restuls return with that username
                        $check_rows = $user_query->rowCount();
                        
                        // If there is a row then notify user that name already exists
                        if ($check_rows >=1) {
                            echo "<br><b>Username Already Exisits!</b>";
                            die();
                        }
                        
                        // If the name doesnt exist add to the DB
                        else {
                            // Creates a hash value to store the password in, for added security
                            $pwhash = password_hash($password, PASSWORD_DEFAULT);
                        
                            // Prepare the Insert statement
                            $query = $pdo->prepare("INSERT INTO users (username, password, email, DOB, gender) VALUES ('$username', '$pwhash', '$email','$dob','$gender')");

                            // Run the statement. If successful, a popup will appear confirming registration
                            $query->execute();

                        
                            echo "<script type='text/javascript'>
                                document.getElementById('pageTitle').innerHTML='Registration complete!';
                                document.getElementById('pageInfo').innerHTML='Your registration has been successful.';
                                document.getElementById('rego').innerHTML='';
                            </script>";
                        }
                    }
                }
            ?>

        </main>

        <!-- Content after the main -->
        <?php include 'php/after_main.inc' ?>

    </body>
</html>