
<?php
	// Start the login session
	session_start();
?>

<!DOCTYPE html>

<html lang="en">
    <head>
        <!-- Important Author and Description Information -->
        <meta charset="utf-8">
        <meta name="description" content="Net Tracker utilises Brisbane City Councils open data initiative to load data on wifi hotspots around Brisbane. We then provide a platform for users to leave reviews on each location.">
        <meta name="author" content="Brendan Surtees & Kalebh Harwin">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--  Title and External Links  -->
        <title>Net Tracker | Search Results</title>
        <link rel="stylesheet" type="text/css" href="CSS/main.css">
        <link rel="stylesheet" type="text/css" href="CSS/searchresults.css">

        <!-- JavaScript Link -->
        <script src="JavaScript/main.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB86ADoIvm-TDLPTPY_rLNUrAc7hEftL0E&callback=searchMap" async defer></script>

        <!-- Fonts and Favicon Link -->
        <link rel="icon" type="image/png" href="images/favicon.png">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        
        <!-- OpenGraph -->
        <meta property="og:site_name" content="Net Tracker"/>
        <meta property="og:title" content="Search results"/>
        <meta property="og:description" content="Net Tracker shows you all the best free wifi locations in the city of Brisbane."/>
        <meta property="og:type" content="Review"/>
        <meta property="og:url" content="searchresults.php"/>  
        
    <?php 
        
        function clear() {
            header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
        }
    ?>
        
    </head>


    <body>

        <!-- Content before the main -->
        <?php include 'php/location_page_before_main.inc' ?>
        
        <!-- Main Map Search Results -->
        <div id="map" style="height: 60vh; width: 100%;"></div>
        
        <!-- Main page content -->
        <main>         
            <!-- Connect to DB -->
            <?php include 'db_connect.php' ?>

            <h1>Search Results</h1>

            <script>
				// Setting and array for the markers used in the map later
                var features = [];
            </script>
            
            <?php
                // Get the query that was searched on the last page
                $searchquery = $_GET['searchquery'];
                $suburbfilter = $_GET['suburb'];
                $ratingfilter = $_GET['rating'];
                $distance = $_GET['distance'];
                $latLocal = $_GET['latitude'];
                $longLocal = $_GET['longitude'];

				// Creating the query with options to include each of the filters
                $query = "SELECT hotspotName, address, suburb, lat, longi FROM dataset 
                    WHERE `suburb` LIKE '" . $suburbfilter . "%' ";
                    
					// Rating filter checker
                    if ($ratingfilter != ""){
                        $query .= "AND `rating` LIKE '%" . $ratingfilter . "%'";
                    }
                    
                    $query .= "AND (`hotspotName` LIKE '%" . $searchquery . "%' OR `address` LIKE '%" . $searchquery . "%' OR `suburb` LIKE '%" . $searchquery . "%')  ";

                $searchresults = $pdo->query($query);

                // How many rows does the search return?
                $rownum = $searchresults->rowCount();

                if ($searchquery == "" && $suburbfilter == "" && $ratingfilter == "" && $distance == ""){
                    echo 'Your search returned no results. Try broadening your search.';
                }

                else {
                    // Counts number of rows returned from search and prints. 
                    echo '<p style="text-align:center;">
                                '. $rownum . ' results returned.
                                <br /><br />
                                <a href="index.php">←</a>
                            </p>';

                    // Print each search result
                    foreach ($searchresults as $result){
                        $latitudeRes = $result['lat'];
                        $longitudeRes = $result['longi'];

                        if ($latLocal == 0) {
                            $km = ' ';
                            $textKm = ' ';
                        }
                        
                        else {
                            // Determining the distance in Km the user is away from the park
                            // Code Sourced from https://www.geodatasource.com/developers/php
                            $theta = $longLocal - $longitudeRes;
                            $dist = sin(deg2rad($latLocal)) * sin(deg2rad($latitudeRes)) +  cos(deg2rad($latLocal)) * cos(deg2rad($latitudeRes)) * cos(deg2rad($theta));
                            $dist = acos($dist);
                            $dist = rad2deg($dist);
                            $km = round($dist * 60 * 1.1515 * 1.609344);
                            $textKm = ' Km from you.';
                        }

						// Printing each result with the distance from the user
                        if ($km <= $distance) {
                            echo '<a href="wifilocation.php?locationName=' .$result['hotspotName'] .'&addressName=' .$result['address'] .'&suburbName=' .$result['suburb'] .'&latitude=' .$result['lat'] .' &longitude=' .$result['longi'] .'"><article class="comments">
                            <aside>
                                <h2>' . $result['hotspotName'] . '</h2>
                                <p>' . $result['address'] . '<br />' . $result['suburb'] . '</p>
                                <p><i>'.$km.$textKm.'</i></p>
                            </aside>
                            <img src="Images/map.svg" alt="map" class="map_icon_search">
                            </article></a>
                            
                            <script>
                                features.push(['.$result['lat'].','.$result['longi'].',"'.$result['hotspotName'].'","wifilocation.php?locationName=' .$result['hotspotName'] .'&addressName=' .$result['address'] .'&suburbName=' .$result['suburb'] .'&latitude=' .$result['lat'] .' &longitude=' .$result['longi'].'"]);
                            </script>
                            ';
                        }
                        
						// Printing each result without the distance from the user if they have turned it off
                        if ($distance == '') {
                            echo '<a href="wifilocation.php?locationName=' .urlencode($result['hotspotName']) .'&addressName=' . urlencode($result['address']) .'&suburbName=' . urlencode($result['suburb']) .'&latitude=' .$result['lat'] .'&longitude=' .$result['longi'] .'"><article class="comments">
                            <aside>
                                <h2>' . $result['hotspotName'] . '</h2>
                                <p>' . $result['address'] . '<br />' . $result['suburb'] . '</p>
                                <p><i>'.$km.$textKm.'</i></p>
                            </aside>
                            <img src="Images/map.svg" alt="map" class="map_icon_search">
                            </article></a>
                            
                            <script>
                                features.push(['.$result['lat'].','.$result['longi'].',"'.$result['hotspotName'].'","wifilocation.php?locationName=' .$result['hotspotName'] .'&addressName=' .$result['address'] .'&suburbName=' .$result['suburb'] .'&latitude=' .$result['lat'] .' &longitude=' .$result['longi'].'"]);
                            </script>
                            ';
                        }
                    } 
                }
            ?>
            
            <script>
                // Creates the map
                // Code sourced from Google - https://developers.google.com/maps/documentation/javascript/examples/marker-simple
                var map;
                function searchMap() {
                  map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 11,
                    center: new google.maps.LatLng(-27.4698, 153.0251),
                    disableDefaultUI: true,
                    icon: 'images/marker.png'
                  });
          
                // Create markers.
                  features.forEach(function(feature) {
                    var marker = new google.maps.Marker({
                      position: {lat:feature[0], lng:feature[1]},
                      title: feature[2],
                      map: map,
                      icon: 'images/marker.png'
                    });
                    
                    // Linking markers to pages
                    google.maps.event.addListener(marker, 'click', function() {
                    window.location.href = feature[3];
                    });
                  });
                  clear()
                }
            </script>
        </main>

        <!-- Content after the main -->
        <?php include 'php/after_main.inc' ?>

    </body>
</html>