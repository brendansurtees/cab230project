/*  Java Scrip File */

/*  Header Change */
window.onscroll = function headerChange(){
        var newHeader = document.getElementById('header');
        var scrollPosY = window.pageYOffset | document.body.scrollTop;
        if(scrollPosY > 80) {
                newHeader.className = ('header desktop-header active_header');   /*  <-- Add a class to the header to change background */
        } else if(scrollPosY <= 80) {
                newHeader.className =  ('header desktop-header');    /*  <-- Remove a class to the header to change background */
        }
}


/*  Mobile Nav Menu */
function toggle_menu() {
    var btnsToHide = document.getElementsByClassName("hidden-btns");
    
    for (var i = 0; i < btnsToHide.length; i++)
    {
        if (btnsToHide[i].style.display === "block") {
            btnsToHide[i].style.display = "none";      /*  <-- Add a class to the buttons to reveal the new menu style */
        } else {
            btnsToHide[i].style.display = "block";       /*  <-- Remove a class to the buttons to reveal the old menu style */
        }
    }    
}

/*  Search Filter */
function toggle_filter() {
        var toggle = document.getElementById("filter_options");
                if (toggle.style.display === "block") {
                    toggle.style.display = "none";
                } else {
                    toggle.style.display = "block";
                }
}

/*  Geolocation */
/* Adapted from https://www.w3schools.com/html/html5_geolocation.asp */
function userLocation() {
        var geo = document.getElementById("location_coords");
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else { 
            geo.innerHTML = "Geolocation is not supported by this browser.";
        }
}

/* Setting the value of the filter of search to the users coordinates */
function showPosition(position) {
        var geo = document.getElementById("coord1");
        var geo1 = document.getElementById("coord2");
        
        geo.setAttribute("value", position.coords.latitude);
        geo1.setAttribute("value", position.coords.longitude);    
}





