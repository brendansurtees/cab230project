
<?php
	// Start the login session
	session_start();
?>

<!DOCTYPE html>

<html lang="en">
    <head>
        <!-- Important Author and Description Information -->
        <meta charset="utf-8">
        <meta name="description" content="Net Tracker utilises Brisbane City Councils open data initiative to load data on wifi hotspots around Brisbane. We then provide a platform for users to leave reviews on each location.">
        <meta name="author" content="Brendan Surtees & Kalebh Harwin">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--  Title and External Links  -->
        <title>Net Tracker | About</title>
        <link rel="stylesheet" type="text/css" href="CSS/main.css">
        <link rel="stylesheet" type="text/css" href="CSS/other.css">

        <!-- JavaScript Link -->
        <script src="JavaScript/main.js"></script>

        <!-- Fonts and Favicon Link -->
        <link rel="icon" type="image/png" href="images/favicon.png">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        
        <!-- OpenGraph -->
        <meta property="og:site_name" content="Net Tracker"/>
        <meta property="og:title" content="Net Tracker | About Us"/>
        <meta property="og:description" content="Net Tracker shows you all the best free wifi locations in the city of Brisbane."/>
        <meta property="og:type" content="Review"/>
        <meta property="og:url" content="about.php"/>
    </head>

    <body>
        <!-- Content before the main -->
        <?php include 'php/before_main.inc' ?>

        <!-- Main page content -->
        <main>
            <h1>About Us</h1>
            <p>Net tracker is a free public service website for users to find WiFi hotspots near them. We access Brisbane City Councils WiFi hotspot data set which is part of the open data initiative provided by the council.
                Users can search for locations directly or add in filters to refine their search to places around them.<br>When searching via proximity be sure that you have allowed your browser to accept location services from this website otherwise the search will not work properly.<br><br>
                Make sure you register to our website to keep up to date with all of the new WiFi hotspots being added to Brisbane and leave comments on locations you visit.</p>
            
			<?php
				// Check to see if the user is logged in
				if ($_SESSION["login"] == "false") {
					// If they arent then display the register button
					echo '<form action="register.php">
							<div class="formItem">
								<button name="register" type="submit" value="about.php">Register</button>
							</div>
						</form>';
				}
			?>
        </main>


        <!-- Content after the main -->
        <?php include 'php/after_main.inc' ?>

    </body>
</html>