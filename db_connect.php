<?php
    // Set information relevant for connecting to the database
    define('DBHOST','cab230.sef.qut.edu.au');
    define('DBUSER','n9878076');
    define('DBPASS','password');
    define('DBNAME','n9878076');

    // Try and connect to the database
    try {
        $pdo = new PDO('mysql:host='.DBHOST.';port=3306;dbname='.DBNAME, DBUSER, DBPASS);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // If connection fails, throw an error message
    } catch (PDOException $e) {
        print "Database connection error: " . $e->getMessage() . "<br/>";
        die();
    }
?>