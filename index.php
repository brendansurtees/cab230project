
<?php
	// Start the login session
	session_start();
?>

<!DOCTYPE html>

    <html lang="en">
    <head>
        <!-- Important Author and Description Information -->
        <meta charset="utf-8">
        <meta name="description" content="Net Tracker utilises Brisbane City Councils open data initiative to load data on wifi hotspots around Brisbane. We then provide a platform for users to leave reviews on each location.">
        <meta name="author" content="Brendan Surtees & Kalebh Harwin">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--  Title  -->
        <title>Net Tracker | Home</title>
		
		<!-- External CSS Links -->
        <link rel="stylesheet" type="text/css" href="CSS/main.css">
        <link rel="stylesheet" type="text/css" href="CSS/index.css">

        <!-- JavaScript Link -->
        <script src="JavaScript/main.js"></script>
        
        <!-- App Manifest -->
        <link rel="manifest" href="/manifest.json">
        
        <!-- Fonts and Favicon Link -->
        <link rel="icon" type="image/png" href="images/favicon.png">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        
        <!-- OpenGraph -->
        <meta property="og:site_name" content="Net Tracker"/>
        <meta property="og:title" content="Net Tracker | Home"/>
        <meta property="og:description" content="Net Tracker shows you all the best free wifi locations in the city of Brisbane."/>
        <meta property="og:type" content="Review"/>
        <meta property="og:url" content="index.php"/>

    </head>

    <body>
        <!-- Desktop header -->
        <header class="header desktop-header" id="header">
            <!-- Website Logo -->
            <a href="index.php"><img id="desktop-menu-img" alt="Net Tracker Logo & Home Button" src="images/logo.svg"/></a>
                
            <!-- Main Nav / Left -->
            <nav>
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">About</a></li>
                </ul>
            </nav>
			
            <!-- Rego Nav / Right -->
            <nav class="rego_nav">
                <ul>
                    <?php
						// Check to see if they are logged in
                        if ($_SESSION["login"] == "true") {
							// If they are give them the option to logout
                            echo '<form method="POST">
                                    <li>
                                        <button name="logout" type="submit" class="logout_button">Logout</button>
                                    </li>
                                </form>';
                                
                            // When they logout write to the session they are logged out
                            if (isset($_POST['logout'])) {
                                $_SESSION["login"] = "false";
                                header("Refresh:0");
                            }
                        }
                        else {
							// If not logged in display the login and register buttons
                            echo '<li ><a href="login.php">Login</a></li>
                            <li class="nav_right_link"><a href="register.php">Register</a></li>';
                        }
                    ?>
                </ul>
            </nav>                
        </header>
    
        <!-- Mobile header -->
        <header class="mobile-header">
            <nav>
                <ul>
					<!-- Hamburger (Drop down) Menu -->
                    <li id="menu-li">
                        <a href="#"><img id="menu-img" src="images/menu-icon.png" alt="Dropdown menu" onclick="toggle_menu()" />
                        </a>
                    </li>
                    <li><a href="index.php"><img src="images/logo.png" class="mobile_logo" alt="Net Tracker Logo"></a></li>
                    <li class="hidden-btns" id="home-button-mob"><a href="index.php">Home</a></li>
                    <li class="hidden-btns"><a href="about.php">About</a></li>
                    <?php
						// Check to see if they are logged in
                        if ($_SESSION["login"] == "true") {
							// If they are give them the option to logout
                            echo '<form method="POST">
                                    <li class="hidden-btns">
                                        <button name="logout" type="submit" class="logout_button">Logout</button>
                                    </li>
                                </form>';
                                
                            // When they logout write to the session they are logged out
                            if (isset($_POST['logout'])) {
                                $_SESSION["login"] = "false";
                                header("Refresh:0");
                            }
                        }
                        else {
							// If not logged in display the login and register buttons
                            echo '<li class="hidden-btns"><a href="login.php">Login</a></li>
                            <li class="hidden-btns"><a href="register.php">Register</a></li>';
                        }
                    ?>
                </ul>
            </nav>
        </header>


        <!-- Landing Banner -->
        <div class="banner">
            <!-- - Search Bar - -->
            <form class="search_bar_holder" action="searchresults.php" method="GET">
                <input type="text" class="search_bar" placeholder="Search..." name="searchquery" />
                
				<!-- Search Button-->
                <button class="filters" id="button_divider" type="submit">
                    <img class="search_button_icons" src="Images/search.svg" alt="Search">
                </button>
				<!--Display the filters / options-->
                <button onclick="toggle_filter(); userLocation(); return false;" class="filters" type="submit">
                    <img class="search_button_icons" src="Images/order.svg" alt="Filters">
                </button>
                <!-- Filter Options -->
                    <div id="filter_options">
                        <div class="filter_containers">

                            <!-- Suburb filter -->
                            <label>Suburb</label><br>

                            <!-- Connect to DB -->
                            <?php include 'db_connect.php' ?>
                            
                            <!-- Get suburb names straight from database -->
                            <select name="suburb">
                                <option value="">-</option>
                                <?php 
                                    // SQL query to get list of suburbs
                                    $suburblist = $pdo->prepare("SELECT DISTINCT suburb FROM dataset");
                                    $suburblist->execute();
                                    
                                    // Populate the form list with suburbs
                                    foreach ($suburblist as $suburb){
                                        echo "<option value='". $suburb['suburb'] ."'>" . $suburb['suburb'] . "</option>";
                                    }
                                ?>
                            </select>
                        </div>

						<!--Search By Rating-->
                        <div class="filter_containers">
                            <label>Rating</label><br>
                            <select name="rating">
                                <option value="">-</option>
                                <option value="5">5</option>
                                <option value="4">4</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                        </div>
                        
						<!--Search By Distance-->
                        <div class="filter_containers">
                            <label>Sort by Distance</label><br>
                            <select name="distance">
                                <option value="">-</option>
                                <option value="1">1km</option>
                                <option value="5">5km</option>
                                <option value="10">10km</option>
                                <option value="20">20km</option>
                            </select>
                        </div>

						<!-- Users Coordinates (filled in by JavaScript)-->
                        <div id="geo_filter_containers">
                            <label>Your Coordinates</label><br>
                            <input id="coord1" name="latitude" type="text" value="0"><br>
                            <input id="coord2" name="longitude" type="text" value="0">
                        </div>
                    </div>
                </form>
            
        </div>

        <!--  Main page content  -->
        <main>
            <h1>Featured WiFi Locations</h1>
            <a href="wifilocation.php?locationName=City%20Botanic%20Gardens%20Wifi&addressName=Alice%20Street&suburbName=Brisbane%20City&latitude=-27.47561%20&longitude=153.03005"><article id="feature1">
                <aside id="feature_text1">
                    <h3>Brisbane City Botanical Gardens</h3>
                </aside>
                <img src="Images/gardens.jpg" alt="City Botanical Gardens" id="feature_image1">
            </article></a>
            
            <a href="wifilocation.php?locationName=Kenmore%20Library%20Wifi&addressName=Kenmore%20Village%20Shopping%20Centre,%20Brookfield%20Road&suburbName=Kenmore,%204069&latitude=-27.50592852%20&longitude=152.9386437"><article id="feature2">
                <aside id="feature_text2">
                    <h3>Kenmore State Library</h3>
                </aside>
                <img src="Images/kenmore.jpg" alt="Kenmore Library" id="feature_image2">
            </article></a>
            
            <a href="wifilocation.php?locationName=Post%20Office%20Square&addressName=Queen%20&%20Adelaide%20Sts&suburbName=Brisbane&latitude=-27.46735%20&longitude=153.02735"><article id="feature3">
                <aside id="feature_text3">
                    <h3>Brisbane City Post Office Square</h3>
                </aside>
                <img src="Images/postoffice.jpg" alt="City Post Office Square" id="feature_image3">
            </article></a>
            <p>Check out this weeks features WiFi hotspots and make sure you leave a review when you visit them. We choose featured locations which often have higher level reviews and which showcase some of brisbanes unique locations. If you want to find WiFi hotspots near you make sure you use our search function to find what you are looking for.</p>
        </main>

        
        <!-- Content after the main -->
        <?php include 'php/after_main.inc' ?>

    </body>
</html>